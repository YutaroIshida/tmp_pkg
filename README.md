# これは？
ZCU102でCNN（Convolutional Neural Network）の推論デモを行うためのROSパッケージ．  
PCで画像を読み込み，ZCU102に転送し，推論を行う．

# 機材
- ROS（ディストリビューションは問わない）がインストールされたPC
- ZCU102
- SDカード
- LANケーブル

# 事前準備
- 適当なROSワークスペースに本ROSパッケージを入れ，ビルドしておく

# 使用方法
- PCをLANケーブルまたはWiFiを介してネットワークに接続する
- ZCU102をLANケーブルを介してPCと同一ネットワークに接続する  
（標準状態で，ZCU102のネットワーク設定はDHCPにしている．ローカル接続に変更する場合，下記ネットワーク設定の変更方法を参照する．）
- PCとZCU102 JB3 UART1をUSBで接続する
- ZCU102にSDカードを挿入する
- ZCU102の電源を入れ，INIT LED2つが赤から緑に変化し点灯することを確認する
- 新しいPCのターミナルを開く
- PCのターミナルで  $　ifconfig  を実行しeth0?? PCのIPアドレスをメモする
- 新しいPCのターミナルを開く
- PCのターミナルで  $ sudo screen /dev/ttyUSB0 115200  を実行しZCU102のターミナルを表示する
- ZCU102のターミナルで  $　ifconfig  を実行しeth0 ZCU102のIPアドレスをメモする
- 新しいPCのターミナルを開く
- PCのターミナルで  $ source PATH_TO_ROS_WORKSPACE/devel/setup.bash  を実行する
- PCのターミナルで  $ rosrun tmp_pkg server  を実行する
- 新しいPCのターミナルを開く
- PCのターミナルで  $ ssh linaro@ZCU102のIPアドレス  を実行しZCU102のターミナルを表示する（パスワードはpassword）
- ZCU102のターミナルで  $　su - root  を実行する（パスワードはpassword）
- ZCU102のターミナルで  $ cd /home/linaro/cnn  を実行する
- ZCU102のターミナルで  $ ./cnn 192.168.0.1（コマンドライン引数にPCのIPアドレスを与える）  を実行する
- PCのターミナルに真値のクラスラベル，ZCU102のターミナルに推論されたクラスラベルが表示されます，2つが一致していると嬉しいです．
（ZCU102のターミナル，一番下に表示されるのがクラスラベル，Eval on HWの下に表示されるのが推論時間です．）

# ネットワーク設定の変更方法
# ローカル接続に変更する場合
- PCの/home/etc/network/interfacesを以下の記述に変更する

```
auto lo
iface lo inet loopback

allow-hotplug eth0（OSによってはeth0ではない）

iface eth（OSによってはeth0ではない） inet static
address 192.168.0.1
gateway 192.168.0.254
netmask 255.255.255.0
```

- SDカードの/rootfs/home/etc/network/interfacesを以下の記述に変更する

```
auto lo
iface lo inet loopback

allow-hotplug eth0

iface eth inet static
address 192.168.0.2
gateway 192.168.0.254
netmask 255.255.255.0
```

# 注意事項
- プログラム実行中に死んだ場合，プログラムを再起動してもソケットのエラーが残る時があります．PCとZCU102を再起動してください．
