#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace std;

#include <ros/ros.h>
#include <ros/package.h>

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

int main(
    int argc,
    char **argv
)
{
    //string ip_addr(argv[1]);

    //変数宣言
    int server_sock;
    struct sockaddr_in server_addr;

    int client_sock;
    struct sockaddr_in client_addr;

    socklen_t len = sizeof(struct sockaddr_in);
 

    //ソケット接続処理
    if((server_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        cout << "err1" << endl;
    }
 
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(60124);
    server_addr.sin_addr.s_addr = INADDR_ANY;
 
    if((bind(server_sock, (struct sockaddr *)&server_addr, len)) < 0){
        cout << "err2" << endl;
    }

    cout << "Wait for clinet" << endl;
    if(listen(server_sock, SOMAXCONN) < 0){
        cout << "err3" << endl;
    }
 
    if((client_sock = accept(server_sock, (struct sockaddr *)&client_addr, &len)) < 0){
        cout << "err4" << endl;
    }


    //平均画像の読み込み
    cv::Mat mean;
    mean = cv::imread(ros::package::getPath("tmp_pkg") + "/src/rgbd/mean.jpg");
    //cv::imshow("window", mean);
    //cv::waitKey(1);

    //cout << (int)mean.at<cv::Vec3b>(0, 0)[2] << endl; //R
    //cout << (int)mean.at<cv::Vec3b>(0, 0)[1] << endl; //G
    //cout << (int)mean.at<cv::Vec3b>(0, 0)[0] << endl; //B


    //推論画像の送信
    int data[224 * 224 * 3];

    while(1){
        for(int m = 0; m < 2; m++){ //低速・高速モード
            for(int i = 0; i < 51; i++){ //51クラス
                for(int j = 0; j < 5; j++){ //5枚
                    cv::Mat img;

                    ostringstream ss;
                    ss << ros::package::getPath("tmp_pkg") + "/src/rgbd/";
                    ss << i;
                    ss << "/";
                    ss << j;
                    ss << ".png";

                    img = cv::imread(ss.str());
                    cv::imshow("window", img);
                    cv::waitKey(1);

                    cout << "True label: " << i << endl;

                    //min + (int)(rand() * (max - min +1.0)/(1.0 + RAND_MAX));
                    int ran = 0 + (int)(rand() * (30 - 0 + 1.0)/(1.0 + RAND_MAX));
                    cv::Mat clipped_mean(mean, cv::Rect(ran, ran, 224, 224));
                    cv::Mat clipped_img(img, cv::Rect(ran, ran, 224, 224));

                    for(int y = 0; y < clipped_img.rows; y++){
                        for(int x = 0; x < clipped_img.cols; x++){
                            data[224 * 3 * y + 3 * x + 0] = clipped_img.at<cv::Vec3b>(y, x)[2] - clipped_mean.at<cv::Vec3b>(y, x)[2]; //R
                            data[224 * 3 * y + 3 * x + 1] = clipped_img.at<cv::Vec3b>(y, x)[1] - clipped_mean.at<cv::Vec3b>(y, x)[1]; //G
                            data[224 * 3 * y + 3 * x + 2] = clipped_img.at<cv::Vec3b>(y, x)[0] - clipped_mean.at<cv::Vec3b>(y, x)[0]; //B
                        }
                    }

                    if(send(client_sock, (char *)data, sizeof(data), 0) < 0){
                        cout << "err5" << endl;
                    }

                    if(m == 0){
                        cv::waitKey(500);
                    }
                }
            }
        }
    }

    close(client_sock);
    close(server_sock);

    return 0;
}
